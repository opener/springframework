package com.fz.entity;

import lombok.Data;

/**
 * Created by Administrator on 2017/7/3.
 */
//@Component("uok")//注解，还可以用controller,service
//@Service("uok")
@Data
public class User {

//    public User(){
//        System.out.println("构造方法（）......");
//    }
//    @PostConstruct//初始化注解
//    public void init(){
//        System.out.println("init ..... 初始化...");
//    }
//
//    @PreDestroy
//    public void destory(){
//        System.out.println("销毁方法。。。。");
//    }

    private Student student;
    private Book book;
    public void show(){
        System.out.println("数据显示.....");
        System.out.println(student.getName());
        System.out.println(book.getName());
    }

//    @PostConstruct//初始化注解
//    public void abc(){
//        System.out.println("abc....method");
//    }

}
