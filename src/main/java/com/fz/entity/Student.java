package com.fz.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Administrator on 2017/7/1.
 */
@Data @AllArgsConstructor @NoArgsConstructor
//@Component("sok")
public class Student {
    private int id;
    private String name;

    //@PostConstruct//初始化注解
    public void init1(){
        System.out.println("init1 ..... 初始化...");
    }
}
