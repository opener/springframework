package com.fz.entity;

import lombok.Data;

/**
 * Created by Administrator on 2017/7/3.
 */
@Data
public class Book {
    private int sno;
    private String name;
}
