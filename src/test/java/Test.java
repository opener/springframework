/**
 * Created by Administrator on 2017/7/1.
 */
public class Test {
    private String name;

    //私有化构造方法
    private Test() {
        this.name = name;
    }

    //一个方法
     public  int pf(int i){
        return i*i;
     }

     //本类中new一个对象
    private static Test tt = new Test();

     //提供一个访问该对象的方法
    public static Test gets(){
        return tt;
    }
}
