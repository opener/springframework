import com.fz.Hello;
import com.fz.entity.Student;
import com.fz.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2017/7/1.
 */
public class Demo {

    protected BeanFactory factory;
    @Before
    public void init(){
        this.factory = new ClassPathXmlApplicationContext("beans.xml");//这里面可以填数组，直接引入多个配置文件
    }

    @Test
    public void ttt(){
        User u = this.factory.getBean("user",User.class);
        u.show();
    }

    @Test
    public void uuu(){
        //User u = this.factory.getBean("users",User.class);

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        User u = ctx.getBean("uok",User.class);
        ctx.close();

       // User u1 = this.factory.getBean("uok",User.class);
        //Student u2 = this.factory.getBean("sok",Student.class);
    }

    @Test
    public void iii(){
//        Hello h = this.factory.getBean("hh",Hello.class);
//        h.show();
//        Hello l = this.factory.getBean("hl",Hello.class);
//        l.show();

//        Hello h = this.factory.getBean("hhh",Hello.class);
//        h.show();
//        h.showstudent();
//        System.out.println("***********");
//        Hello h1 = this.factory.getBean("hhh1",Hello.class);
//        h1.show();
//        h1.showstudent();
//        System.out.println("***********");
//        Hello h2 = this.factory.getBean("hhh2",Hello.class);
//        h2.show();
//        h2.showstudent();

//        SimpleDateFormat sdf = this.factory.getBean("sdf",SimpleDateFormat.class);
//        Date d = this.factory.getBean("d",Date.class);
//        System.out.println(sdf.format(d));

        Hello h2 = this.factory.getBean("h10",Hello.class);
        h2.show();
        h2.showstudent();
        System.out.println(h2.getStus().size());
        for(Student s:h2.getStus()){
            System.out.println(s.getName());
        }
        System.out.println("*******");
        for(String ss: h2.getAddr()){
            System.out.println(ss);
        }

    }

    @Test
    public  void sp(){
        //ClassPathXmlApplicationContext ctx  = new ClassPathXmlApplicationContext("beans.xml"); //和下面的差不多，有继承关系
        BeanFactory factory = new ClassPathXmlApplicationContext("beans.xml");
        Hello h = factory.getBean("h",Hello.class);
        Hello h1 = factory.getBean("h",Hello.class);
        System.out.println(h);
        System.out.println(h.pf(5));
        System.out.println(h==h1);

        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = factory.getBean("d", Date.class);
        System.out.println(d);
        System.out.println(dateFormater.format(d));
    }
}

